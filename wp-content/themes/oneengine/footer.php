<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package OneEngine
 */
?>

	</div><!-- #content -->
	<div class="clearfix"></div>
	<footer id="contact" class="site-footer template-wrap" role="contentinfo">
		<div class="col-md-3">
			<div class="footer-content carta">
				<img src="http://localhost/wordpress/wp-content/uploads/2016/08/Carta.png" alt="Carta"class="aligncenter size-full wp-image-240" />
			</div>
			<div class="footer-content text">contato@ghsistema.com</div>	
		</div>
		<div class="col-md-3">
			<div class="footer-content tel">
				<img src="http://localhost/wordpress/wp-content/uploads/2016/08/Tel.png" alt="Tel" class="aligncenter size-large wp-image-241" />
			</div>
			<div class="footer-content text">(55 21) 4042-7280</div>
		</div>
		<div class="col-md-6">
			<div class="footer-right">		
				<img src="http://localhost/wordpress/wp-content/uploads/2016/08/clicksoft.png"/>
			</div>
			<div class="footer-right" >
				<?php 
					$languages = pll_the_languages(array('raw'=>1));
					foreach ($languages as $language){
						if($language['slug'] == 'pt' && $language['current_lang']){
							$desenvolvido = 'Desenvolvido por'; 
						}
						elseif($language['slug'] == 'en' && $language['current_lang']){
							$desenvolvido = 'Developed by'; 
						}
					}
						
					echo $desenvolvido;
				?>
			</div>
	</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>
<script src="http://localhost/wordpress/wp-includes/js/custom.js?" type="text/javascript"></script>
